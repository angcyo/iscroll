# Roadmap

## Dev environment
- [ ] Add browser testing
- [ ] Add linter (es6, jshint)

## Refactoring
- [ ] Totally moves to ES6
- [ ] Add alias variables to improve minification

## Improvements
- [ ] fps.js: Add `strictdom` to report DOM perfomance leaks
- [ ] fps.js: Rework read/write frame loop.

## Testing
- [ ] Cover EventEmitter.js
- [ ] Finish Covering RenderLayer.js

## RenderLayer
- [x] Add mousewheel
- [x] Add magicpad support
- [ ] Snap
- [ ] ScrollIndicator
- [ ] Pull to refresh support

